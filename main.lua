function love.load()
    love.graphics.setNewFont(50)

    box = {
        x = love.graphics.getWidth()/2,
        y = love.graphics.getHeight()/2,
        width = 250,
        height = 250,
        rotation = 0,
        r = 1,
        g = 1,
        b = 0,
    }
end

function love.update(dt)
    box.rotation = box.rotation + 2*dt
    box.width = (1+math.sin(box.rotation))/2*250 + 200
    box.height = (1+math.cos(box.rotation))/2*250 + 200
    box.r = (1 + math.cos(box.rotation*0.123))/2 + 0.1
    box.g = (1 + math.sin(box.rotation*0.753))/2 + 0.1
    box.b = (1 + math.cos(box.rotation*1.227))/2 + 0.1
end

function love.draw()
    love.graphics.setBackgroundColor(box.r * 0.4, box.g * 0.4, box.b * 0.4)
    love.graphics.push()

    -- Draw box
    love.graphics.setColor(box.r, box.g, box.b)
    love.graphics.translate(box.x, box.y)
    love.graphics.rotate(box.rotation)
    love.graphics.rectangle('fill', -box.width/2, -box.height/2, box.width, box.height)

    -- Draw text
    local ty = math.sin(box.rotation)*50
    love.graphics.setColor(0, 0, 0)
    love.graphics.printf('boon', -box.x+2, 2, love.graphics.getWidth(), 'center')
    love.graphics.setColor(255, 255, 255)
    love.graphics.printf('boon', -box.x, 0, love.graphics.getWidth(), 'center')
    love.graphics.rotate(math.pi)
    love.graphics.setColor(0, 0, 0)
    love.graphics.printf('boon', -box.x+2, 2, love.graphics.getWidth(), 'center')
    love.graphics.setColor(255, 255, 255)
    love.graphics.printf('boon', -box.x, 0, love.graphics.getWidth(), 'center')
    love.graphics.pop()
end
